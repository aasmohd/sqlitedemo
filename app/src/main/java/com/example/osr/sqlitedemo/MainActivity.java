package com.example.osr.sqlitedemo;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements OnClickListener {

	EditText name, email, address;
	Button save, show, delete;
	ListView show_data_list;

	Database_Helper database;

	// Variable for database table fields size
	int data_size = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Calling init method
		init();
	}

	void init() {
		name = (EditText) findViewById(R.id.name);
		email = (EditText) findViewById(R.id.email);
		address = (EditText) findViewById(R.id.address);

		save = (Button) findViewById(R.id.save);
		show = (Button) findViewById(R.id.show);
		delete = (Button) findViewById(R.id.delete);

		show_data_list = (ListView) findViewById(R.id.show_saved_data);

		// Initializing database
		database = new Database_Helper(MainActivity.this);

		// Implementing click listener to all buttons
		save.setOnClickListener(this);
		show.setOnClickListener(this);
		delete.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.save:

			// Getting edittext fields texts into string
			String Name = name.getText().toString();
			String Email = email.getText().toString();
			String Address = address.getText().toString();

			// Check if all edit text is filled or not
			if (TextUtils.isEmpty(Name) || TextUtils.isEmpty(Email)
					|| TextUtils.isEmpty(Address)) {

				// Showing a toast if any of them fields empty
				Toast.makeText(MainActivity.this, "All fields are necessary.",
						Toast.LENGTH_SHORT).show();
			} else {

				// Insert data into database
				database.insertData(new Data_Model(Name, Email,
					Address));
              //  database.insertData(Name,Email,Address); like this create problem
                // coz with this now u need Data_model instance in Database_helper
                // and if we create Data_model instance in Database_helper it asks for
                //***constructor arguments

				name.setText("");
				email.setText("");
				address.setText("");

				// Toast for successfully saved data
				Toast.makeText(MainActivity.this, "Data saved successfully.",
						Toast.LENGTH_SHORT).show();

			}

			break;

		case R.id.show:

			// Getting stored data from database in list
			ArrayList<Data_Model> list = database.getAllData();

			// getting list size and stored in data_size
			data_size = list.size();

			// Checked if database contains data or not
			if (data_size == 0) {

				// If data is not contained means 0 then show a toast
				Toast.makeText(MainActivity.this, "There is no data in table.",
						Toast.LENGTH_SHORT).show();

				// Check if listview is shown or not
				if (show_data_list.isShown()) {

					// Hide listview
					show_data_list.setVisibility(View.GONE);
				}
			} else {


				// Custom adapter for setting array list
				Custom_Adapter adapter = new Custom_Adapter(MainActivity.this,
						list);

				// setting adapter over listview
				show_data_list.setAdapter(adapter);

				// Notifying adapter
				adapter.notifyDataSetChanged();

				// Showing listview
				show_data_list.setVisibility(View.VISIBLE);
			}

			break;

		case R.id.delete:

			// Check if database contains data or not
			if (data_size == 0) {

				// If there is no data toast is shown
				Toast.makeText(MainActivity.this,
						"There is nothing to delete.", Toast.LENGTH_SHORT)
						.show();
			} else {

				// else data is deleted and hide the listview
				Toast.makeText(MainActivity.this, "Data deleted Successfully.",
						Toast.LENGTH_SHORT).show();
				database.deleteTable();
				show_data_list.setVisibility(View.GONE);
			}

			break;
		}

	}

}
